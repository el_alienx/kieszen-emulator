export default class RAM {
  private _buffer: ArrayBuffer
  private _uint8array: Uint8Array
  private readonly _size: number

  constructor (size: number) {
    this._size = size
    this._buffer = new ArrayBuffer(size)
    this._uint8array = new Uint8Array(this._buffer, 0, size)
  }

  // # API
  // Function
  public clearData () {
    this._buffer = new ArrayBuffer(this._size)
    this._uint8array = new Uint8Array(this._buffer, 0, this._size)
  }

  // Functional
  public setData (address: number, data: number) {
    this._uint8array[address] = data
  }

  // Functional
  public getData (address: number): number {
    return this._uint8array[address]
  }

  // Functional
  public getSection (start: number, end: number): Uint8Array {
    return this._uint8array.slice(start, end)
  }
}
