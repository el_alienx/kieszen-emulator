export default class Display {
  private _canvas: HTMLCanvasElement

  constructor (x: number, y: number) {
    this._canvas = document.createElement('canvas')
    this._canvas.id = 'Screen'
    this._canvas.width = x
    this._canvas.height = y
  }

  // # API
  // Pure
  get canvas (): HTMLCanvasElement {
    return this._canvas
  }

  get canvasContext (): CanvasRenderingContext2D {
    const context = this.canvas.getContext('2d', { alpha: false })
    const error = 'The display canvas does not have a 2d context'

    if (context === null) { throw Error(error) }
    return context
  }
}
