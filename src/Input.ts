// eslint-disable-next-line no-unused-vars
import RAM from 'RAM'

export default class Input {
  private readonly _ram: RAM
  private readonly _inputAddress: number

  // to cache button press/realsee methods
  // Refactor init to 0
  private _currentValue = NaN
  private _newValue = NaN

  constructor (ram: RAM, inputAddress: number) {
    this._ram = ram
    this._inputAddress = inputAddress
  }

  // # API
  public start (): void {
    this._listenKeyboard()
  }

  // # Methods
  // Pure
  private _listenKeyboard () {
    document.onkeydown = (event) => {
      if (event.code === 'ArrowUp') { this.buttonPress(128) } // binary 1000_0000
      if (event.code === 'ArrowDown') { this.buttonPress(64) } // binary 0010_0000
      if (event.code === 'ArrowLeft') { this.buttonPress(32) } // binary 0001_0000
      if (event.code === 'ArrowRight') { this.buttonPress(16) } // binary 0100_0000
      if (event.code === 'Enter') { this.buttonPress(8) } // binary 0000_1000
      if (event.code === 'KeyZ') { this.buttonPress(4) } // binary 0000_0100
      if (event.code === 'KeyX') { this.buttonPress(2) } // binary 0000_0010
      if (event.code === 'KeyC') { this.buttonPress(1) } // binary 0000_0001
    }

    document.onkeyup = (event) => {
      if (event.code === 'ArrowUp') { this.buttonRelease(128) } // binary 1000_0000
      if (event.code === 'ArrowDown') { this.buttonRelease(64) } // binary 0010_0000
      if (event.code === 'ArrowLeft') { this.buttonRelease(32) } // binary 0001_0000
      if (event.code === 'ArrowRight') { this.buttonRelease(16) } // binary 0100_0000
      if (event.code === 'Enter') { this.buttonRelease(8) } // binary 0000_1000
      if (event.code === 'KeyZ') { this.buttonRelease(4) } // binary 0000_0100
      if (event.code === 'KeyX') { this.buttonRelease(2) } // binary 0000_0010
      if (event.code === 'KeyC') { this.buttonRelease(1) } // binary 0000_0001
    }
  }

  // Pure
  private buttonPress (value: number) {
    this._currentValue = this._ram.getData(this._inputAddress)
    this._newValue = this._currentValue | value // bitwise operation (binary OR)

    this._ram.setData(this._inputAddress, this._newValue)
  }

  // Pure
  private buttonRelease (value: number) {
    this._currentValue = this._ram.getData(this._inputAddress)
    this._newValue = this._currentValue ^ value // bitwise operation (binary XOR)

    this._ram.setData(this._inputAddress, this._newValue)
  }
}
