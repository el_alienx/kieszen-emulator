import AddressMode from './AddressModes.js'
import FlagCheck from './FlagCheck.js'
import MemoryMap from '../../node_modules/kieszen-core/src/MemoryMap.js'
// eslint-disable-next-line no-unused-vars
import RAM from '../RAM.js'
// eslint-disable-next-line no-unused-vars
import Registers from './Registers.js'
import ConvertNumber from '../../node_modules/kieszen-core/src/ConvertNumber.js'
import ErrorEvent from '../../node_modules/kieszen-core/src/ErrorEvent.js'

export default abstract class Operations {
  // To cache various operations
  private static _oldA = 0
  private static _programCounter = 0
  private static _value = 0
  private static _longAddress = ''
  private static _rawLByte = ''
  private static _rawHByte = ''
  private static _parsedLByte = 0
  private static _parsedHByte = 0
  private static _stackAddress = 0
  private static _stackPointer = 0

  // # API
  // Pure
  public static execute (registers: Registers, ram: RAM, mnemonic: string): Registers {
    // @ts-ignore
    if (this[mnemonic] === undefined) ErrorEvent.emit(10, 'Emulator', mnemonic, `The operation with mnemonic ${mnemonic} is not implemented`)
    // @ts-ignore
    return this[mnemonic](registers, ram)
  }

  // # Methods
  private static adc (registers: Registers, ram: RAM): Registers {
    // Note: we need to edit the Accumulator directly
    // because we need the verification limit that
    // it perform.

    // Operations
    this._oldA = registers.A
    registers.A = registers.A + registers.MD

    // Flags
    registers.Flags.carry = (registers.A > 255)
    registers.Flags.negative = FlagCheck.checkNegative(registers.A)
    registers.Flags.overflow = FlagCheck.checkOverflow(this._oldA, registers.MD)
    registers.Flags.zero = (registers.A === 0)

    return registers
  }

  private static and (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.A = (registers.A & registers.MD)

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.A)
    registers.Flags.zero = (registers.A === 0)

    return registers
  }

  private static bcc (registers: Registers, ram: RAM): Registers {
    // Operation
    if (!registers.Flags.carry) registers.PC = registers.MA

    return registers
  }

  private static bcs (registers: Registers, ram: RAM): Registers {
    // Operation
    if (registers.Flags.carry) registers.PC = registers.MA

    return registers
  }

  private static beq (registers: Registers, ram: RAM): Registers {
    // Operation
    if (registers.Flags.zero) registers.PC = registers.MA

    return registers
  }

  private static bmi (registers: Registers, ram: RAM): Registers {
    // Operation
    if (registers.Flags.negative) registers.PC = registers.MA

    return registers
  }

  private static bne (registers: Registers, ram: RAM): Registers {
    // Operation
    if (!registers.Flags.zero) registers.PC = registers.MA

    return registers
  }

  private static bpl (registers: Registers, ram: RAM): Registers {
    // Operation
    if (!registers.Flags.negative) registers.PC = registers.MA

    return registers
  }

  private static bvc (registers: Registers, ram: RAM): Registers {
    // Operation
    if (!registers.Flags.overflow) registers.PC = registers.MA

    return registers
  }

  private static bvs (registers: Registers, ram: RAM): Registers {
    // Operation
    if (registers.Flags.overflow) registers.PC = registers.MA

    return registers
  }

  private static brk (registers: Registers, ram: RAM): Registers {
    // Flags
    registers.Flags.break = true

    return registers
  }

  private static clc (registers: Registers, ram: RAM): Registers {
    // Flags
    registers.Flags.carry = false

    return registers
  }

  private static cli (registers: Registers, ram: RAM): Registers {
    // Flags
    registers.Flags.interrupt = false

    return registers
  }

  private static clv (registers: Registers, ram: RAM): Registers {
    // Flags
    registers.Flags.overflow = false

    return registers
  }

  private static cpa (registers: Registers, ram: RAM): Registers {
    // Flags
    registers.Flags.carry = (registers.A >= registers.MD)
    registers.Flags.negative = FlagCheck.checkNegative(registers.A - registers.MD)
    registers.Flags.zero = (registers.A === registers.MD)

    return registers
  }

  private static cpx (registers: Registers, ram: RAM): Registers {
    // Flags
    registers.Flags.carry = (registers.X >= registers.MD)
    registers.Flags.negative = FlagCheck.checkNegative(registers.X - registers.MD)
    registers.Flags.zero = (registers.X === registers.MD)

    return registers
  }

  private static cpy (registers: Registers, ram: RAM): Registers {
    // Flags
    registers.Flags.carry = (registers.Y >= registers.MD)
    registers.Flags.negative = FlagCheck.checkNegative(registers.Y - registers.MD)
    registers.Flags.zero = (registers.Y === registers.MD)

    return registers
  }

  private static dex (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.X--

    // Flags
    registers.Flags.zero = (registers.X === 0)
    registers.Flags.negative = FlagCheck.checkNegative(registers.X)

    return registers
  }

  private static dey (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.Y--

    // Flags
    registers.Flags.zero = (registers.Y === 0)
    registers.Flags.negative = FlagCheck.checkNegative(registers.Y)

    return registers
  }

  private static eor (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.A = (registers.A ^ registers.MD)

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.A)
    registers.Flags.zero = (registers.A === 0)

    return registers
  }

  private static inx (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.X++

    // Flags
    registers.Flags.zero = (registers.X === 0)
    registers.Flags.negative = FlagCheck.checkNegative(registers.X)

    return registers
  }

  private static iny (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.Y++

    // Flags
    registers.Flags.zero = (registers.Y === 0)
    registers.Flags.negative = FlagCheck.checkNegative(registers.Y)

    return registers
  }

  private static jmp (registers: Registers, ram: RAM): Registers {
    registers.PC = registers.MA

    return registers
  }

  private static jsr (registers: Registers, ram: RAM): Registers {
    // Operations
    this._value = registers.PC + 1
    this._longAddress = ConvertNumber.decimalToHex(this._value)
    this._rawLByte = ConvertNumber.lowByte(this._longAddress)
    this._rawHByte = ConvertNumber.highByte(this._longAddress)
    this._parsedLByte = parseInt(this._rawLByte, 16)
    this._parsedHByte = parseInt(this._rawHByte, 16)

    ram.setData(MemoryMap.stack.start + registers.SP + 0, this._parsedLByte)
    ram.setData(MemoryMap.stack.start + registers.SP + 1, this._parsedHByte)

    registers.PC = registers.MA
    registers.SP += 2

    return registers
  }

  private static lda (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.A = registers.MD

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.A)
    registers.Flags.zero = (registers.A === 0)

    return registers
  }

  private static ldx (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.X = registers.MD

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.X)
    registers.Flags.zero = (registers.X === 0)

    return registers
  }

  private static ldy (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.Y = registers.MD

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.Y)
    registers.Flags.zero = (registers.Y === 0)

    return registers
  }

  private static ora (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.A = (registers.A | registers.MD)

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.A)
    registers.Flags.zero = (registers.A === 0)

    return registers
  }

  private static pha (registers: Registers, ram: RAM): Registers {
    // Operation
    ram.setData(MemoryMap.stack.start + registers.SP, registers.A)
    registers.SP++

    return registers
  }

  private static pla (registers: Registers, ram: RAM): Registers {
    // Operation
    this._stackAddress = MemoryMap.stack.start + registers.SP - 1
    registers.A = ram.getData(this._stackAddress)
    ram.setData(this._stackAddress, 0)
    registers.SP--

    // Flags
    registers.Flags.zero = (registers.A === 0)
    registers.Flags.negative = FlagCheck.checkNegative(registers.A)

    return registers
  }

  private static sbc (registers: Registers, ram: RAM): Registers {
    // Note: we need to edit the Accumulator directly
    // because we need the verification limit that
    // it perform.

    // Operations
    this._oldA = registers.A
    registers.A = registers.A - registers.MD

    // Flags
    registers.Flags.carry = !(registers.A > 255) // invert for substraction
    registers.Flags.negative = FlagCheck.checkNegative(registers.A)
    registers.Flags.overflow = FlagCheck.checkOverflow(this._oldA, registers.MD)
    registers.Flags.zero = (registers.A === 0)

    return registers
  }

  private static sec (registers: Registers, ram: RAM): Registers {
    // Flags
    registers.Flags.carry = true

    return registers
  }

  private static sei (registers: Registers, ram: RAM): Registers {
    // Flags
    registers.Flags.interrupt = true

    return registers
  }

  private static sta (registers: Registers, ram: RAM): Registers {
    // Operations
    registers.MD = registers.A
    ram.setData(registers.MV, registers.MD)

    return registers
  }

  private static stx (registers: Registers, ram: RAM): Registers {
    // Operations
    registers.MD = registers.X
    ram.setData(registers.MV, registers.MD)

    return registers
  }

  private static sty (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.MD = registers.Y
    ram.setData(registers.MV, registers.MD)

    return registers
  }

  private static rts (registers: Registers, ram: RAM): Registers {
    // Operation
    this._stackPointer = registers.SP + MemoryMap.stack.start - 2
    this._programCounter = AddressMode.getLongAddress(this._stackPointer, ram)

    ram.setData(this._stackPointer + 0, 0)
    ram.setData(this._stackPointer + 1, 0)

    registers.SP -= 2
    registers.PC = this._programCounter - 1

    return registers
  }

  private static tax (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.X = registers.A

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.X)
    registers.Flags.zero = (registers.X === 0)

    return registers
  }

  private static tay (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.Y = registers.A

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.Y)
    registers.Flags.zero = (registers.Y === 0)

    return registers
  }

  private static txa (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.A = registers.X

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.A)
    registers.Flags.zero = (registers.A === 0)

    return registers
  }

  private static tya (registers: Registers, ram: RAM): Registers {
    // Operation
    registers.A = registers.Y

    // Flags
    registers.Flags.negative = FlagCheck.checkNegative(registers.A)
    registers.Flags.zero = (registers.A === 0)

    return registers
  }
}
