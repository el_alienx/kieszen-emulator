export default abstract class FlagCheck {
  // To cache checkSign()
  private static _isSigned = false

  // #API
  // Pure
  public static checkNegative (register: number): boolean {
    return (register & 128) === 128
  }

  // Pure
  public static checkOverflow (firstValue: number, secondValue: number): boolean {
    this._isSigned = false

    if (firstValue < 128 && secondValue < 128) {
      if (firstValue + secondValue > 127) this._isSigned = true
    }

    return this._isSigned
  }
}
