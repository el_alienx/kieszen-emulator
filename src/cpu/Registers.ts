// eslint-disable-next-line no-unused-vars
import IFlag from '../interfaces/IFlag'
import MemoryMap from '../../node_modules/kieszen-core/src/MemoryMap.js'

export default class Registers {
  private _A = 0
  private _X = 0
  private _Y = 0
  private _SP = 0
  private _PC = 0
  private _MA = 0
  private _MV = 0
  private _MD = 0
  private _Flags: IFlag = {
    negative: false,
    overflow: false,
    break: false,
    decimal: false,
    interrupt: false,
    zero: false,
    carry: false
  }

  constructor () {
    this._PC = MemoryMap.programCode.start
  }

  // # API
  get A (): number { return this._A }
  get X (): number { return this._X }
  get Y (): number { return this._Y }
  get SP (): number { return this._SP }
  get PC (): number { return this._PC }
  get MA (): number { return this._MA }
  get MV (): number { return this._MV }
  get MD (): number { return this._MD }
  get Flags (): IFlag { return this._Flags }

  set A (value: number) { this._A = this._limit(value) }
  set X (value: number) { this._X = this._limit(value) }
  set Y (value: number) { this._Y = this._limit(value) }
  set SP (value: number) { this._SP = this._limit(value) }
  set PC (value: number) { this._PC = value }
  set MA (value: number) { this._MA = value }
  set MV (value: number) { this._MV = value }
  set MD (value: number) { this._MD = value }
  set Flags (value: IFlag) { this._Flags = value }

  // # Methos
  // Pure
  private _limit (value: number): number {
    if (value > 255) value -= 256
    if (value < 0) value += 256

    return value
  }
}
