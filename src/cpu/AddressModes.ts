import RAM from '../RAM.js'
import Registers from './Registers.js'
import ErrorEvent from '../../node_modules/kieszen-core/src/ErrorEvent.js'

export default abstract class AddressMode {
  // To cache getLongAddress()
  private static readonly _HIGH_BYTE_CONVERTER = 256
  private static _byteL = 0
  private static _byteH = 0

  // To cache getValue()
  private static _value = 0

  // To cache various address modes
  private static _indirection = 0
  private static _longAddress = 0

  // # API
  // Pure
  public static getAddress (programCounter: number, bytes: number, ram: RAM): number {
    if (bytes === 1) { return -1 } // result = no address
    if (bytes === 2) { return programCounter + 1 }
    if (bytes === 3) { return this.getLongAddress(programCounter + 1, ram) }

    throw new ErrorEvent(8, `${programCounter}`, 'Emulator', `The operand length ${bytes} is invalid at PC ${programCounter}`)
  }

  // Pure
  public static getValue (addressMode: string, registers: Registers, ram: RAM): number {
    // this[addressMode] is a method call by parsing addressMode as string

    // @ts-ignore
    if (this[addressMode] === undefined) { throw new ErrorEvent(9, 'Emulator', `${addressMode} at @${registers.PC}`, `Address mode ${addressMode} not found`) }
    // @ts-ignore
    return this[addressMode](registers, ram)
  }

  // Pure
  public static getLongAddress (address: number, ram: RAM): number {
    this._byteL = ram.getData(address)
    this._byteH = ram.getData(address + 1) * this._HIGH_BYTE_CONVERTER

    return this._byteH + this._byteL
  }

  // # Methods
  // Pure
  private static implied (registers: Registers, ram: RAM): number {
    return registers.MV
  }

  // Pure
  private static accumulator (registers: Registers, ram: RAM): number {
    return registers.MV
  }

  // Pure
  private static stack (registers: Registers, ram: RAM): number {
    return registers.MV
  }

  // Pure
  private static immediate (registers: Registers, ram: RAM): number {
    return ram.getData(registers.MA)
  }

  // Pure
  private static zeroPage (registers: Registers, ram: RAM): number {
    return ram.getData(registers.MA)
  }

  // Pure
  private static zeroPageX (registers: Registers, ram: RAM): number {
    return ram.getData(registers.MA) + registers.X
  }

  // Pure
  private static zeroPageY (registers: Registers, ram: RAM): number {
    return ram.getData(registers.MA) + registers.Y
  }

  // Pure
  private static indirectX (registers: Registers, ram: RAM): number {
    this._indirection = registers.MA + registers.Y
    this._longAddress = this.getLongAddress(this._indirection, ram)

    return this._longAddress
  }

  // Pure
  private static indirectY (registers: Registers, ram: RAM): number {
    this._longAddress = this.getLongAddress(registers.MA, ram)
    this._indirection = this._longAddress + registers.Y

    return this._indirection
  }

  // Pure
  private static absolute (registers: Registers, ram: RAM): number {
    return registers.MA
  }

  // Pure
  private static absoluteX (registers: Registers, ram: RAM): number {
    return registers.MA + registers.X
  }

  // Pure
  private static absoluteY (registers: Registers, ram: RAM): number {
    return registers.MA + registers.Y
  }

  // NOT TESTED
  private static absoluteIndirect (registers: Registers, ram: RAM): number {
    return registers.MA
  }
}
