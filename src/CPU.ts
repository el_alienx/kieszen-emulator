import AddressMode from './cpu/AddressModes.js'
// eslint-disable-next-line no-unused-vars
import IOpcode from '../node_modules/kieszen-core/src/interfaces/IOpcode'
import OpcodeTable from '../node_modules/kieszen-core/src/OpcodeTable.js'
import Operations from './cpu/Operations.js'
// eslint-disable-next-line no-unused-vars
import RAM from './RAM.js'
import Registers from './cpu/Registers.js'

export default class CPU {
  private _opcode: IOpcode
  private _ram: RAM
  private _registers: Registers

  // To cache fetch()
  private _rawOpcode = 0

  constructor (ram: RAM) {
    this._opcode = { opcode: '', opcodeDecimal: NaN, mnemonic: '', addressMode: '', addressCode: 0, bytes: 0 }
    this._ram = ram
    this._registers = new Registers()
  }

  // # API
  // Functional
  get opcode (): IOpcode {
    return this._opcode
  }

  // Functional
  get registers (): Registers {
    return this._registers
  }

  // Impure
  // mutate state
  public reset () {
    this._registers = new Registers()
  }

  // Functional
  public step () {
    this._executeInstruction()
  }

  // Impure
  // Depend of state
  public cycle () {
    if (this._registers.Flags.break === false) this._executeInstruction()
  }

  // # Methods
  // Impure
  // mutate state, depend of state
  private _executeInstruction () {
    // Fetch
    this._opcode = this._fetch(this._registers, this._ram)

    // Decode
    this._registers = this._decode(this._registers, this._ram, this._opcode)

    // Execute
    this._registers = this._execute(this._registers, this._ram, this._opcode.mnemonic)
  }

  // Pure
  private _fetch (registers: Registers, ram: RAM): IOpcode {
    this._rawOpcode = ram.getData(registers.PC)

    return OpcodeTable.getCommand(this._rawOpcode)
  }

  // Pure
  private _decode (registers: Registers, ram: RAM, opcodeData: IOpcode): Registers {
    registers.MA = AddressMode.getAddress(registers.PC, opcodeData.bytes, ram)
    registers.MV = AddressMode.getValue(opcodeData.addressMode, registers, ram)
    registers.MD = ram.getData(registers.MV)
    registers.PC += opcodeData.bytes

    // Special cases
    if (opcodeData.addressMode === 'immediate') registers.MD = registers.MV

    return registers
  }

  // Pure
  private _execute (registers: Registers, ram: RAM, mnemonic: string): Registers {
    return Operations.execute(registers, ram, mnemonic)
  }
}
