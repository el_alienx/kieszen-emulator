import CPU from './CPU.js'
import Display from './Display.js'
import Input from './Input.js'
import RAM from './RAM.js'
import VPU from './VPU.js'
import MemoryMap from '../node_modules/kieszen-core/src/MemoryMap.js'

export default class Emulator {
  private _cpuCycles = 0
  private _request = 0
  private _status = 0 // 0 = stop, 1 = run, 2 = break

  private readonly _cpu: CPU
  private readonly _display: Display
  private readonly _input: Input
  private readonly _vpu: VPU
  private readonly _workRAM: RAM

  private readonly _callBack: Function

  constructor (ramAvailable: number, callBack: Function) {
    this._display = new Display(200, 160)
    this._workRAM = new RAM(ramAvailable)
    this._cpu = new CPU(this._workRAM)
    this._input = new Input(this._workRAM, MemoryMap.hardwareInput)
    this._vpu = new VPU(this._display.canvasContext, this._workRAM)
    this._callBack = callBack

    this._input.start()
  }

  // # API
  get display (): Display {
    return this._display
  }

  // Functional
  public getMemorySection (index: number): Uint8Array {
    return this._workRAM.getSection(
      MemoryMap.sections[index].start,
      MemoryMap.sections[index].start + MemoryMap.sections[index].bytes
    )
  }

  public getMemoryData (address: number): number {
    return this._workRAM.getData(address)
  }

  public setROM (bytes: Uint8Array, spriteSheets: object[], backgroundSheets: object[]): void {
    const ram = this._workRAM
    const codeStart = MemoryMap.programCode.start

    ram.clearData()
    bytes.forEach((item, index) => { ram.setData(index + codeStart, item) })

    // Test (refactor later)
    this._vpu.setGraphics(spriteSheets, backgroundSheets)
  }

  // Functional
  public start (): void {
    this._status = 1
    this._cpu.reset()
    this._request = requestAnimationFrame(this._executeEmulator.bind(this))
  }

  // Functional
  public step (): void {
    this._status = 1
    this._cpu.step()
    this._vpu.cycle()
    this._updateLightAPI()
    this.stop()
  }

  // Fuctional
  public stop (): void {
    this._status = 0
    this._updateLightAPI()
    cancelAnimationFrame(this._request)
  }

  // Fuctional
  public resume (): void {
    this._status = 1
    this._cpu.registers.Flags.break = false
    this._request = requestAnimationFrame(this._executeEmulator.bind(this))
  }

  // # Methods
  // Functional
  private _executeEmulator (): void {
    // Run CPU
    for (this._cpuCycles = 0; this._cpuCycles < 500; this._cpuCycles++) {
      this._cpu.cycle()
    }

    // Draw one frame
    this._vpu.cycle()

    // handle break
    if (this._cpu.registers.Flags.break) {
      this._breakInterrupt()
      return
    }

    this._workRAM.setData(MemoryMap.hardwareInterrupt, 0) // clear interrupt
    this._updateLightAPI()
    this._request = requestAnimationFrame(this._executeEmulator.bind(this))
  }

  // Functional
  private _breakInterrupt (): void {
    this._status = 2
    cancelAnimationFrame(this._request)
  }

  // Impure, mutate other class
  private _updateLightAPI () {
    this._callBack({
      opcode: this._cpu.opcode,
      status: this._status,
      registers: {
        A: this._cpu.registers.A,
        X: this._cpu.registers.X,
        Y: this._cpu.registers.Y,
        SP: this._cpu.registers.SP,
        MV: this._cpu.registers.MV,
        Flags: this._cpu.registers.Flags
      }
    })
  }
}
