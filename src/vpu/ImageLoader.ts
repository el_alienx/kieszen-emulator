export default class ImageLoader {
  private readonly _image: HTMLImageElement

  constructor (imageURL: string) {
    this._image = this._loadImage(imageURL)
  }

  // # API
  // Pure
  get image (): HTMLImageElement {
    return this._image
  }

  // # Methods
  // Pure
  private _loadImage (imageURL: string): HTMLImageElement {
    // This step should load the image from the base64 string on the project.zen.
    // If the file over there don't exist of is corrupted, then it should load the default one.
    const image = new Image()
    const error = 'The image failed to load'

    image.src = imageURL
    image.onerror = () => { throw Error(error) }
    return image
  }
}
