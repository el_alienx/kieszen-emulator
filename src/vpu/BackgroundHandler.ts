// eslint-disable-next-line no-unused-vars
import RAM from '../RAM.js'
import ImageLoader from './ImageLoader.js'
import TextureCoordinates from './TextureCoordinates.js'
// eslint-disable-next-line no-unused-vars
import IBGTile from '../interfaces/IBGTile'
import MemoryMap from '../../node_modules/kieszen-core/src/MemoryMap.js'

export default class SpriteHandler {
  private readonly _RAM: RAM
  private readonly _TILE_SIZE: number = 8
  private readonly _TILE_COLUMS = 50
  private readonly _TILE_ROWS = 40
  private readonly _TOTAL_TILES = this._TILE_COLUMS * this._TILE_ROWS
  private _backgroundSheet: ImageLoader
  private _backgroundTiles: IBGTile[] = []
  private _layer: HTMLCanvasElement

  // To cache updateBackgroundTiles()
  private _tileIndex = 0
  private _currentTileId = 0
  private _bgTile: IBGTile = { id: 0, tile: document.createElement('canvas'), atlasX: 0, atlasY: 0 }
  private _index = 0

  // To cache getTile()
  private _tileCanvas = document.createElement('canvas')
  private _coordinates: number[] = []

  constructor (ram: RAM, imageURL: string) {
    this._RAM = ram
    this._backgroundSheet = new ImageLoader(imageURL) // The class image loader should only return a image

    this._backgroundTiles = this._initBGTiles(this._backgroundTiles)
    this._layer = document.createElement('canvas')
    this._layer.width = this._TILE_SIZE * this._TILE_COLUMS
    this._layer.height = this._TILE_SIZE * this._TILE_ROWS

    // To cache getTile()
    this._tileCanvas.width = this._TILE_SIZE
    this._tileCanvas.height = this._TILE_SIZE
  }

  // # API
  get layer (): HTMLCanvasElement {
    this._layer = this._updateBackgroundTiles(this._layer, this._backgroundTiles, this._RAM)
    return this._layer
  }

  // Purity unknow
  public updateImage (base64: string) {
    this._backgroundSheet = new ImageLoader(base64)
    this._initBGTiles(this._backgroundTiles) // force to refresh cache
  }

  // # Methods
  private _initBGTiles (bgTiles: IBGTile[]): IBGTile[] {
    let index = 0
    let atlasX = 0
    let atlasY = 0

    for (atlasY; atlasY < this._TILE_ROWS; atlasY++) {
      for (atlasX; atlasX < this._TILE_COLUMS; atlasX++) {
        bgTiles[index] = { id: 0, tile: document.createElement('canvas'), atlasX: atlasX * this._TILE_SIZE, atlasY: atlasY * this._TILE_SIZE }
        index++
      }
      atlasX = 0
    }

    return bgTiles
  }

  // Pure
  private _updateBackgroundTiles (canvas: HTMLCanvasElement, bgTiles: IBGTile[], ram: RAM): HTMLCanvasElement {
    for (this._index = 0; this._index < this._TOTAL_TILES; this._index++) {
      this._tileIndex = MemoryMap.videoRAM.start + (this._index * 2) // to skip the metadata memory block
      this._currentTileId = ram.getData(this._tileIndex)
      this._bgTile = bgTiles[this._index]

      if (this._bgTile.id !== this._currentTileId) {
        this._bgTile.id = this._currentTileId
        this._bgTile.tile = this._getTile(this._bgTile.id)

        // @ts-ignore
        canvas.getContext('2d', { alpha: false }).drawImage(this._bgTile.tile, this._bgTile.atlasX, this._bgTile.atlasY)
      }
    }

    return canvas
  }

  private _getTile (index: number): HTMLCanvasElement {
    this._coordinates = TextureCoordinates.matrix[index]

    // @ts-ignore
    this._tileCanvas.getContext('2d', { alpha: false }).drawImage(
      this._backgroundSheet.image, // image = selected textureAtlas
      this._coordinates[0], // sx = tile X position in the spriteSheet
      this._coordinates[1], // sy = tile Y position in the spriteSheet
      this._TILE_SIZE, // sW = tile width in the spriteSheet
      this._TILE_SIZE, // sH = tile height in the spriteSheet
      0, // dx = sprite offset X
      0, // dy = sprite offset Y
      this._TILE_SIZE, // dW = sprite width
      this._TILE_SIZE // dH = sprite height
    )

    return this._tileCanvas
  }
}
