// eslint-disable-next-line no-unused-vars
import RAM from '../RAM.js'
import ImageLoader from './ImageLoader.js'
import TextureCoordinates from './TextureCoordinates.js'
// eslint-disable-next-line no-unused-vars
import ISprite from '../interfaces/ISprite'
import MemoryMap from '../../node_modules/kieszen-core/src/MemoryMap.js'

export default class SpriteHandler {
  private readonly _ram: RAM
  private readonly _tileSize: number
  private _spriteSheet: ImageLoader
  private _sprites: ISprite[]

  // To cache updateSprites()
  private _sprite: ISprite // defined on constructor
  private _loopIndex = 0
  private _spriteIndex = 0
  private _tileId = NaN

  // To cache getTile()
  private _canvas = document.createElement('canvas')
  private _coordinates: number[] = []

  constructor (ram: RAM, imageURL: string) {
    this._ram = ram
    this._spriteSheet = new ImageLoader(imageURL) // The class image loader should only return a image
    this._tileSize = 8
    this._sprites = []
    this._sprites = this._initSprites(this._sprites)

    // To cache updateSprites()
    this._sprite = this._sprites[0]
  }

  // # API
  // Depend on state
  get sprites (): ISprite[] {
    this._sprites = this._updateSprites(this._sprites, this._ram)
    return this._sprites
  }

  // Purity unknow
  public updateImage (base64: string) {
    this._spriteSheet = new ImageLoader(base64)
    this._initSprites(this._sprites) // force to refresh cache
  }

  // # Methods
  // Pure
  private _initSprites (sprites: ISprite[]): ISprite[] {
    for (let i = 0; i < 64; i++) {
      sprites[i] = { id: 0, tile: document.createElement('canvas'), x: 0, y: 0 }
    }

    return sprites
  }

  // Pure
  private _updateSprites (sprites: ISprite[], ram: RAM): ISprite[] {
    for (this._loopIndex = 0; this._loopIndex < 64; this._loopIndex++) {
      this._sprite = sprites[this._loopIndex]
      this._spriteIndex = MemoryMap.oam.start + (this._loopIndex * 4)
      this._tileId = ram.getData(this._spriteIndex)

      // Check if we neeed to redraw the graphic tile
      if (this._sprite.id != this._tileId) {
        this._sprite.id = this._tileId
        this._sprite.tile = this._getTile(this._sprite.id)
      }

      // Update X and Y position
      this._sprite.x = ram.getData(this._spriteIndex + 1)
      this._sprite.y = ram.getData(this._spriteIndex + 2)
    }

    return sprites
  }

  // Get tile
  private _getTile (index: number): HTMLCanvasElement {
    this._canvas = document.createElement('canvas')
    this._canvas.width = this._tileSize
    this._canvas.height = this._tileSize
    this._coordinates = TextureCoordinates.matrix[index]

    this._canvas.getContext('2d')?.drawImage(
      this._spriteSheet.image, // image = selected textureAtlas
      this._coordinates[0], // sx = tile X position in the spriteSheet
      this._coordinates[1], // sy = tile Y position in the spriteSheet
      this._tileSize, // sW = tile width in the spriteSheet
      this._tileSize, // sH = tile height in the spriteSheet
      0, // dx = sprite offset X
      0, // dy = sprite offset Y
      this._tileSize, // dW = sprite width
      this._tileSize // dH = sprite height
    )

    return this._canvas
  }
}
