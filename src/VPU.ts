// eslint-disable-next-line no-unused-vars
import RAM from './RAM.js'
import SpriteHandler from './vpu/SpriteHandler.js'
import BackgroundHandler from './vpu/BackgroundHandler.js'
// eslint-disable-next-line no-unused-vars
import ISprite from './interfaces/ISprite'
import TestImages from '../node_modules/kieszen-core/src/TestImages.js'
import MemoryMap from '../node_modules/kieszen-core/src/MemoryMap.js'

export default class VPU {
  private readonly _canvasContext: CanvasRenderingContext2D
  private readonly _ram: RAM
  private readonly _spriteHandler: SpriteHandler
  private readonly _backgroundHandler: BackgroundHandler

  // To cache drawSpriteLayer()
  private _loopIndex: number = 0
  private _sprite: ISprite = { id: 0, tile: document.createElement('canvas'), x: 0, y: 0 }

  // To cache drawBackgroundLayer()
  private _scrollX = 0
  private _scrollY = 0

  // To cache updateSheets()
  private _newSpriteSheet = 0
  private _selectedSpriteSheet = 0
  private _selectedBackgroundSheet = 0

  // Test (refactor later)
  private _spriteSheets: object[] = []
  private _backgroundSheets: object[] = []

  constructor (canvasContext: CanvasRenderingContext2D, ram: RAM) {
    this._canvasContext = canvasContext
    this._ram = ram
    this._spriteHandler = new SpriteHandler(this._ram, TestImages.spriteSheet)
    this._backgroundHandler = new BackgroundHandler(this._ram, TestImages.backgroundSheet)
  }

  // # API
  // Impure depend on state
  public cycle () {
    this._canvasContext.clearRect(0, 0, this._canvasContext.canvas.width, this._canvasContext.canvas.height)
    this._updateSheets()
    this._scrollBackgroundLayer()
    this._drawSpriteLayer()
  }

  // Purity unknow
  public setGraphics (spriteSheets: object[], backgroundSheets: object[]) {
    // the update happens on the cycle method
    this._spriteSheets = spriteSheets
    this._backgroundSheets = backgroundSheets
  }

  // # Methods
  // Purity unknow
  private _updateSheets () {
    // Sprites
    // 1 Get new value
    this._newSpriteSheet = this._ram.getData(MemoryMap.hardwareSpriteSheet)
    // 2 Check that new value is not out of bounds
    if (this._newSpriteSheet > this._spriteSheets.length) this._newSpriteSheet = 0
    // 3 Check if the value is new
    if (this._selectedSpriteSheet !== this._newSpriteSheet) {
      this._selectedSpriteSheet = this._newSpriteSheet
      // @ts-ignore
      this._spriteHandler.updateImage(this._spriteSheets[this._selectedSpriteSheet].data)
    }

    // Backgrounds
    this._newSpriteSheet = this._ram.getData(MemoryMap.hardwareBackgroundSheet)
    if (this._newSpriteSheet > this._backgroundSheets.length) this._newSpriteSheet = 0
    if (this._selectedBackgroundSheet !== this._newSpriteSheet) {
      this._selectedBackgroundSheet = this._newSpriteSheet
      // @ts-ignore
      this._backgroundHandler.updateImage(this._backgroundSheets[this._selectedBackgroundSheet].data)
    }
  }

  // Impure depened on state
  private _drawSpriteLayer () {
    for (this._loopIndex = 0; this._loopIndex < 64; this._loopIndex++) {
      this._sprite = this._spriteHandler.sprites[this._loopIndex]

      if (this._sprite.id !== 0) this._canvasContext.drawImage(this._sprite.tile, this._sprite.x, this._sprite.y)
    }
  }

  // Purity unknow
  private _scrollBackgroundLayer () {
    this._scrollX = this._ram.getData(MemoryMap.hardwareBackgroundScrollX)
    this._scrollY = this._ram.getData(MemoryMap.hardwareBackgroundScrollY)

    this._canvasContext.drawImage(this._backgroundHandler.layer, -this._scrollX, -this._scrollY)
  }
}
