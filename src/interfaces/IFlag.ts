export default interface IFlag {
  negative: boolean
  overflow: boolean
  break: boolean
  decimal: boolean
  interrupt: boolean
  zero: boolean
  carry: boolean
}
