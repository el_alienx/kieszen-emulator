export default interface IGGTile {
    id: number,
    tile: HTMLCanvasElement
    atlasX: number
    atlasY: number
  }
