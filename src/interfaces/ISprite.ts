export default interface ISprite {
  id: number
  tile: HTMLCanvasElement
  x: number
  y: number
}
