import Assembler from './node_modules/kieszen-assembler/dist/src/Assembler.js'
import Emulator from './dist/src/Emulator.js'
import MemoryMap from './node_modules/kieszen-core/dist/src/MemoryMap.js'

const assembler = new Assembler()
const emulator = new Emulator(MemoryMap.bitsAvailable, callBack)
const container = document.getElementById('canvas-container')
const input = document.getElementById('game-chooser')

container.appendChild(emulator.display.canvas)
input.addEventListener('change', onChange)
autoLoader('foo')

function callBack (object) {
  console.log(object)
}

function autoLoader (url) {
  //
}

function onChange (event) {
  const file = event.target.files[0]
  const reader = new FileReader()

  reader.readAsText(file)
  reader.onload = () => setROM(JSON.parse(reader.result))
}

function setROM (project) {
  const script = project.scripts[0].content
  const byteCode = assembler.getByteCode(script)

  emulator.setROM(byteCode, project.spriteSheets, project.backgroundSheets)
  emulator.start()
}
